package mikrotik

import (
	"log"
	"os"
	"os/exec"
)

type Service interface {
	MTReboot() error
}

type service struct {
}

func New() Service {
	return &service{}
}

func (s *service) mtCommand(command string) error {
	cmd := exec.Command("ssh", "-oStrictHostKeyChecking=no", "-l", s.getEnv("MT_USER"), "-i", "/tmp/id_rsa_bot", s.getEnv("MT_IP"), command)
	log.Println("Mikrotik router rebooting")
	return cmd.Run()
}

func (s *service) MTReboot() error {
	return s.mtCommand("/system reboot")
}

func (s *service) getEnv(env string) string {
	return os.Getenv(env)
}
