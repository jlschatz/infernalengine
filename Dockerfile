FROM golang AS buildStage

WORKDIR /go/src/gitlab.com/jlschatz/infernalengine
COPY . .

RUN go get -insecure golang.org/x/sys/unix
RUN CGO_ENABLED=0  go get -insecure  -v .../.

RUN  CGO_ENABLED=0 go build

FROM alpine

RUN apk --update --no-cache add openssh ca-certificates && update-ca-certificates 2>/dev/null || true

WORKDIR /app
COPY --from=buildStage /go/src/gitlab.com/jlschatz/infernalengine/infernalengine .

EXPOSE 6660
ENTRYPOINT ["/app/infernalengine"]
