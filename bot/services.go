package bot

import (
	"fmt"
	"github.com/sparrc/go-ping"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func (s *service) TestHTTPService(chatID int64, url string) error {

	var netClient = &http.Client{
		Timeout: time.Second * 5,
	}
	if url != "" {
		endpoint, _ := s.store.getEndpoint(url)
		stats, err := netClient.Get(url)
		if err != nil {
			if endpoint.ReportCounter < 10 {
				log.Println(fmt.Sprintf("Unable to access endpoint %s. Please investigate", url))
				s.bot.Send(chatID, fmt.Sprintf("Unable to access endpoint %s. Please investigate", url))
				endpoint.Accessible = false
				endpoint.ReportCounter++
			}
			return err
		}
		defer stats.Body.Close()
		if !strings.Contains(stats.Status, "200") {
			if endpoint.ReportCounter < 10 {
				s.bot.Send(chatID, fmt.Sprintf("An issue with the following endpoint has been detected: %s\nHTTP response code: %s", url, stats.Status))
				endpoint.Accessible = false
				endpoint.ReportCounter++
			}
		}
		if strings.Contains(stats.Status, "200") && endpoint.Accessible == false {
			s.bot.Send(chatID, fmt.Sprintf("The following endpoint is now accessible: %s\nHTTP response code: %s", url, stats.Status))
			endpoint.Accessible = true
		}

		s.store.updateEndpoint(endpoint)
	}
	return nil
}

func (s *service) TestPingService(chatID int64, host string) {

	pinger, err := ping.NewPinger(host)
	if err != nil {
		panic(err)
	}
	pinger.Count = 5
	pinger.Run() // blocks until finished
	stats := pinger.Statistics()

	s.bot.Send(-chatID, fmt.Sprintf("%f", stats.PacketsRecv))
}

func (s *service) SendAlert(groupid int64, message string) {

	s.bot.Send(groupid, message)
}

func (s *service) TestAllSystems() {

	for true {
		allSystems, err := s.store.getEndpoints()

		if err != nil || allSystems == nil {
			s.SendServiceAlert(err)
		} else {
			for _, sys := range allSystems {

				go s.TestHTTPService(sys.GroupID, sys.Endpoint)
			}
		}
		time.Sleep(1 * time.Minute)
	}
}

func (s *service) ReportStoredAlerts() {

	for true {
		storedAlerts, err := s.store.getUnreportedAlerts()
		if err != nil {
			s.SendServiceAlert(err)
		}
		for _, alert := range storedAlerts {
			s.SendAlert(alert.GroupID, alert.Message)
			s.store.updateAlertReportedStatus(alert.Id)
		}
		time.Sleep(5 * time.Second)
	}
}

func (s *service) SendServiceAlert(err error) {

	i, err := strconv.ParseInt(os.Getenv("SERVICE_GROUP"), 10, 64)
	if err != nil {
		log.Println(err)
	}
	s.bot.Send(i, err.Error())
}
