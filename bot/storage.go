package bot

import (
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Store interface {
	getEndpoints() ([]Endpoint, error)
	getAllEndpointsForThisID(groupID int64) ([]Endpoint, error)
	setEndpoint(groupID int64, field, endpoints string) error
	getUnreportedAlerts() ([]Alert, error)
	getEndpoint(endpoint string) (Endpoint, error)
	updateEndpoint(endpoint Endpoint) error
	updateAlertReportedStatus(id bson.ObjectId) error
}

type mongoStore struct {
	mongo *mgo.Database
}

type Endpoint struct {
	Id            string `json:"id" bson:"_id,omitempty"`
	GroupID       int64  `json:"groupid" bson:"groupid"`
	Endpoint      string `json:"endpoint" bson:"endpoint"`
	Host          string `json:"host" bson:"host"`
	Accessible    bool   `json:"accessible" bson:"accessible"`
	ReportCounter int    `json:"reportcounter" bson:"reportcounter"`
}

type Alert struct {
	Id       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Date     time.Time     `json:"date" bson:"date"`
	GroupID  int64         `json:"groupid" bson:"groupid"`
	Message  string        `json:"message" bson:"message"`
	Reported bool          `json:"reported" bson:"reported"`
}

func NewMongoStore(mongo *mgo.Database) Store {
	return &mongoStore{mongo}
}

func (s mongoStore) getEndpoints() ([]Endpoint, error) {
	c := s.mongo.C("Endpoints")
	endpoints := []Endpoint{}
	err := c.Find(nil).All(&endpoints)
	if err != nil {
		return endpoints, err
	}
	return endpoints, nil
}

func (s mongoStore) getEndpoint(endpoint string) (Endpoint, error) {
	c := s.mongo.C("Endpoints")
	system := Endpoint{}
	err := c.Find(bson.M{"endpoint": endpoint}).One(&system)
	if err != nil {
		return system, err
	}
	return system, nil
}

func (s mongoStore) getAllEndpointsForThisID(groupID int64) ([]Endpoint, error) {
	c := s.mongo.C("Endpoints")
	var systems []Endpoint

	err := c.Find(bson.M{"groupid": groupID}).All(&systems)
	if err != nil {
		return systems, err
	}
	return systems, nil
}

func (s mongoStore) setEndpoint(groupID int64, field, endpoint string) error {
	c := s.mongo.C("Endpoints")

	var System Endpoint
	err := c.Find(bson.M{"groupid": groupID, field: endpoint}).One(&System)
	if err == nil {

		er := errors.New("Failed to update details for this client system.")

		return er
	}
	sys := Endpoint{GroupID: groupID, Endpoint: endpoint}
	return c.Insert(sys)
}

func (s mongoStore) updateEndpoint(endpoint Endpoint) error {

	c := s.mongo.C("Endpoints")
	return c.Update(bson.M{"endpoint": endpoint.Endpoint}, bson.M{"$set": bson.M{"accessible": endpoint.Accessible, "reportcounter": endpoint.ReportCounter}})
}

func (s mongoStore) setAlert(groupID int64, message string) error {
	c := s.mongo.C("Alerts")
	alert := Alert{Date: time.Now(), GroupID: groupID, Message: message, Reported: false}
	return c.Insert(alert)
}

func (s mongoStore) getUnreportedAlerts() ([]Alert, error) {
	c := s.mongo.C("Alerts")
	var alerts []Alert

	err := c.Find(bson.M{"reported": false}).All(&alerts)
	if err != nil {
		return alerts, err
	}
	return alerts, nil
}

func (s mongoStore) updateAlertReportedStatus(id bson.ObjectId) error {

	c := s.mongo.C("Alerts")
	return c.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"reported": true}})
}
