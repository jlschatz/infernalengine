package bot

import (
	b64 "encoding/base64"
	"fmt"
	"github.com/yanzay/tbot"
	"io/ioutil"
	"log"
	"os"
)

type Service interface {
	SendAlert(groupid int64, message string)
}

type service struct {
	store Store
	bot   *tbot.Server
}

func RaiseBot(store Store) {
	s := &service{store: store}
	token := os.Getenv("TELEGRAM_TOKEN")
	bot, err := tbot.NewServer(token)
	s.bot = bot
	if err != nil {
		log.Fatal(err)
	}


	bot.HandleFunc("/loadShedding", s.LoadSheddingHandler)
	bot.HandleFunc("/group", s.GroupHandler)
	bot.HandleFunc("/listEndpoints", s.ListEndpointHandler)
	bot.HandleFunc("/keyboard", s.KeyboardHandler)
	bot.HandleFunc("/register {endpoint}", s.AddEndpointHandler)
	bot.HandleFunc("/routerReboot", s.MTRebootHandler)
	bot.HandleDefault(s.UnknownRequestHandler)

	log.Println("With push of Button fire the Engine And spark Turbine into life...Sing Praise to the God of All Machines.")

	s.createPrivateKey()

	go s.TestAllSystems()
	go s.ReportStoredAlerts()

	err = bot.ListenAndServe()
	log.Fatal(err)
}
func (s *service) createPrivateKey() {

		sDec, _ := b64.StdEncoding.DecodeString(os.Getenv("PRIV_KEY"))

		err := ioutil.WriteFile("/tmp/id_rsa_bot", []byte(string(sDec)), 0600)
		if err != nil {
			fmt.Printf("Unable to write file: %v", err)
		}
		log.Println("Private key file created and deployed.")
}

