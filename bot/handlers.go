package bot

import (
	"github.com/kyokomi/emoji"
	"github.com/yanzay/tbot"
	"gitlab.com/jlschatz/infernalengine/mikrotik"
	"log"
	"os"
	"strconv"
	"strings"
)

func (s *service) LoadSheddingHandler(message *tbot.Message) {
	message.Reply(emoji.Sprintf(":bulb: Power supply active :bulb:"))
}

func (s *service) GroupHandler(message *tbot.Message) {
	message.Replyf("Group: %d", message.ChatID)
}

func (s *service) ListEndpointHandler(message *tbot.Message) {
	endpoints, err := s.store.getAllEndpointsForThisID(message.ChatID)
	if err != nil {
		message.Reply("Unable to access list of endpoints.")
	}
	if err == nil && len(endpoints) == 0 {
		message.Reply("No endpoints are currently being monitored.")
	}
	for _, x := range endpoints {
		message.Replyf("%s", x.Endpoint)
	}
}

func (s *service) UnknownRequestHandler(message *tbot.Message) {
	message.Replyf("Nonsensical request '%v'. Please rephrase.", message.Message.Data)
}

func (s *service) KeyboardHandler(message *tbot.Message) {

	if strconv.FormatInt(message.ChatID, 10) != os.Getenv("SERVICE_GROUP"){
		message.Reply("You are not authorized to use this function")
	} else{
		buttons := [][]string{
			{"/loadShedding"},
			{"/routerReboot"},
			{"/listEndpoints"},
		}
		message.ReplyKeyboard("Keyboard options:", buttons)
	}
}

func (s *service) AddEndpointHandler(message *tbot.Message) {

	endpoint := message.Vars["endpoint"]

	if !strings.Contains(message.Vars["endpoint"], "http://") && !strings.Contains(message.Vars["endpoint"], "https://") {
		endpoint = "http://" + message.Vars["endpoint"]
	}

	if err := s.store.setEndpoint(message.ChatID, "endpoint", endpoint); err != nil {
		message.Replyf("%s is already being monitored.", endpoint)
	} else {
		message.Replyf("%s added to the list of monitored endpoints.", endpoint)
	}
}

func (s *service) MTRebootHandler(message *tbot.Message) {

	if strconv.FormatInt(message.ChatID, 10) != os.Getenv("SERVICE_GROUP"){
		message.Reply("You are not authorized to use this function")
	} else {
		mt := mikrotik.New()

		err := mt.MTReboot()

		if err != nil {
			log.Println(err)
			message.Replyf("Mikrotik router reboot failed with the following error: %v", err)
		}
	}
}
